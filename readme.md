# SAM Vorlesung Storage und Speichersysteme
Das Gitlab Repository ist die Quelle um die Vorlesung in verschiedene Formate zu erstellen.


Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vo-storage-speichersysteme/master?grs=gitlab&t=simple

Option 2 (Online-PDF)
  * Über eine CI Pipeline wird mit Pandoc ein PDF zum Download erzeugt dieses kann in der Pipeline heruntergeladen werden.

Option 3 (Lokal):
  * git clone https://gitlab.com/rstumpner/sam-vo-storage-speichersysteme
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Wünsche und Anregungen können gerne in den Gitlab Issues angemerkt werden.
