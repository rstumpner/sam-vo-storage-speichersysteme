---
marp: true
---

## Systemadministration
#### Vorlesung

---
## Storage
### Speichersysteme


---
### Übersicht Speichersysteme
* Arten von Datenträger
* RAID / LVM / JBODs
* DAS / NAS / SAN
* Hochverfügbarkeit von Speichersystemen
* Funktionen von Speichersystemen
* Kennzahlen von Speichersystemen


---
### Arten von Datenträger
* Optische Datenträger
* Magnetische Datenträger
* Elektronische Datenträger


---
### Arten von Datenträgern
#### optisch

#### Durch einen Laser wird auf ein Trägermedium eine Vertiefung eingebrannt , die dadurch entstandene Struktur aus Vertiefung (0) oder Land (1) kann nun optisch wieder Ausgelesen werden

---
### Arten von Datenträger
#### optisch

![Optische Datenträger](_images/medium-optisch-pit.png) ![](_images/medium-optisch.png)

---
### Arten von Datenträgern
#### optisch

* Entwicklung in den 1980er Jahren
* haltbar bis 10 - 100 Jahre
* Begrenzt Wiederbeschreibbar
* Kapazität bis ca. 300GB (2016)
* Übertragungsgeschwindigkeit ca. 36 Mb/s
* Verbreitete Formate CD , DVD , Blue Ray , Archive Disk
  
---
### Arten von Datenträgern
#### optisch (Anwendung)

* Distribution von Software
* im Backup und Archivberiech
* Bezeichnung "Cold Storage"

---
### Arten von Datenträgern
#### magnetisch

#### Durch einen magnetischen Schreib/Lesekopf werden auf einem Trägermaterial Veränderungen mittels eines Magnetfeldes erzeugt oder wahrgenommen (Bitstruktur)

---
### Magnetband
#### Trägermaterial ist ein magnetempfindliches Band dass von einem Gehäuse geschützt wird.

---
### Magnetband

![LTO](_images/1280px-LTO2-cart-purple.jpg)

---
### Magnetband

* erste Entwicklungen 1952 (IBM 7 1/2 Spur)
* viele verschiedene Hersteller und Formate

---
### Magnetband

#### Derzeit ist eigentlich nur mehr der Linear Tape Open (LTO) Standard relevant.

---
### Magnetband 
#### LTO
* Entwicklung als offener Standard
* seit Generation 5
* Metadaten in XML
* Linear Tape File System (LTFS)
* eine Generation Rückwertskompatibel
* Kompression (x2)
* Verschlüsselung (AES-256)
* Write Once Read Many (WORM)
* Aktuell LTO-8 (12TB)
* Übertragungsgeschwindigkeit ca. 360 Mb/s
  
---
### Magnetband 
#### LTO

* Verwendung im Backup Bereich
* Verwendung als Archivmedium
* Verwendung in Datalivecycle Produkten (Objectstore + Policy)
* Bezeichnung "Cold Storage"

---
### Harddisk
##### Mittels eines kleinen Magneten (Schreib/Lese-kopf) wird auf einer magnetisierbaren Scheibe eine Bitstruktur geschrieben

---
### Harddisk
#### magnetisch

![Harddisk Überblick](_images/Hard_drive-de.png)

---
### Harddisk
#### magnetisch

![Harddisk Explusion](_images/medium-magnetisch.png)

---
### Harddisk
#### magnetisch
* Entwicklung in den 1950er Jahren
* Verbreitete Formate 1,8 / 2,5 / 3,5 Zoll
* Wiederbeschreibbar
* Haltbar bis 30 Jahre
* Verschlüsselung (AES)
* Heliumfüllung (GB per inc)
* Übertragungsgeschwindigkeit ca. 200 MB/s
* IOPS 80
* Kapazität bis 16 TB (2019)
* Umdrehungen (7,2k / 10k)



---
### Harddisk
#### magnetisch 

![Aufbau logisch](_images/medium-prinzip.png)

---
### Harddisk
#### magnetisch

* Verwendung im Backup Beriech
* Verwendung in Server und Storage Systemen
* Bezeichnung "Warm Storage" oder "Capacity Tier"


---
### Arten von Datenträgern - elektronisch
#### Daten werden über einen Controller direkt auf einem Halbleiterbaustein Siliziumchip gespeichert

---
### Flash Speicher Aufbau

![Flash Aufbau](_images/flash-aufbau.png)

---
### SSD
#### elektronisch

![](_images/medium-elektronisch.png)

---
### Elektronisch - SSD
* Entwicklung in den 1970er Jahren
* begrenzt Wiederbeschreibbar (ERASE Prozess)
* Haltbar ca. 100.000 Schreibvorgänge
* Formfaktor Speicherkarte , USB-Stick , Festplatte
* Verschlüsselung (AES)
* Kapazität bis 8 TB (2,5 Zoll/2020)
* Übertragungsgeschwindigkeit bis ca. 5 GB/s
* IOPS 100k - 300k

---
### SSD Funktionsweise Logisch

![SSD Logisch](_images/medium-prinzip.png)


---
### Verschiedene Technologien
* Singlelevel Cell (SLC)
* Multilevel Cell (MLC)
* Tripple Level Cell (TLC)
* NV-RAM

---
### Wiederbeschreibbarkeit

| | SLC-Speicher | 	MLC-Speicher |	TLC-Speicher | NV-RAM |
|:---|:---:|:---:|:---:|:---:|
|Bit pro Zelle |	1 |	2 |	3 | |
|Schreibzyklen |	100.000 |	3.000 | 1.000 | unbegrenzt |

---
### Flashspeicher - elektronisch
#### Durch die begrenzte Beschreibbarkeit des Mediums wird viel Wert auf die intelligente Beschreibung des Mediums gelegt

---
### Flashspeicher
#### Lebensdauer 
* Wear Leveling (Verteilung der Schreibzyklen auf die Zellen)
* Controllerfunktionen auf Extra Spezial Chip (ASIC) oder in Software
* Funktionen zur Datenreduktion
* Spezialkonstruktionen im Enterprise Storage Bereich ( Hitachi FMA /
Purestorage Datapacks)

---
### Flashspeicher - elektronisch
#### Trotz sinkender Preise der Speicherzellen ist der Preis pro Terabyte noch zirka 3 x so hoch wie bei den magentischen Harddisks

---
### Funktionen zur Datenreduktion
  * Deduplizierung
  * Kompression
  * Verschlüsselung

---
## Kenzahlen von Speichermedien
* Kapazität pro Quadratzentimeter
* Operationen pro Sekunde (IOPS)
* Datendurchsatz
* Zugriffszeit

---
### Redundant Array of Independent Disks
#### Dient zur Organisation von mehreren unabhängigen physikalischen Festplatten um sie dem Betriebssystem als ein logisches Laufwerk darzustellen

---
### Redundant Array of Independent Disks

* Entwickelt 1988
* Erhöhung des Datendurchsatzes
* der Kapazität 
* der Ausfallsicherheit
* Eingeteilt in verschiedene RAID Level
* gibt’s als Hardare oder Software Lösung
* oft optimierte Überprüfung der geschriebenen Daten

---
### RAID Levels
* RAID 0 – Striping
* RAID 1 – Spiegel
* RAID 5 – Striping + Parity
* RAID 6 – Striping + 2 x Parity
* Kombinationen von den oben genannten
* Details unter http://de.wikipedia.org/wiki/RAID

---
### RAID 6 (Doppelte Parität)
![RAID 6](_images/850px-RAID_6.svg.png)


---
### Hardware RAID
* eigener Prozessor und Spezialchips (ASIC / FPGA)
* eigene Firmware (Software)
* meist eigener Cache Speicher (ECC)
* Battery Backup Unit
* Hot-Swap fähigkeit
* Für das Betriebssystem als eine Disk Sichtbar
* Vorsicht Hybrid-Controller
* Inkompatibel zu manchen Herstellern

---
### Software RAID
* keine Spezielle Hardware nötig
* Schicht zwischen Dateisystem und Hardware ( Kernelmodul / Treiber)
* Nutzung der Rechner Resourcen ( CPU / Memory)
* Geschwindigkeitseinbußen bei einem Rebuild
* Manchmal schwierig bei Boot Vorgängen
* Datenintegrität bei Stromausfall ist nicht gegeben

---
## RAID Datensicherheit

* Sparedisk (Cold / Hot / Prefill)
* Disk / Data Scrubbing


---
### Sparedisks
#### Zum Bestehenden RAID Array wird eine zusätzliche Disk benötigt

---
### Sparedisks

* Cold
* Hot
* Hot with Prefill

---
### Sparedisks
#### Cold

Die Ersatzdisk beim Ausfall wird sofort getauscht (extern)

---
### Sparedisks
#### Hot

Die Ersatzdisk beim Ausfall wird automtisch verwendet (intern)

---
### Sparedisks
#### Hot with Prefill

Bei einer Medium Fehlererkennung (SMART) wird die Ersatzdisk schon vor dem tatsächlichen Ausfall befüllt

---
### Disk / Data Scrubbing

##### RAID Systeme bieten die Möglichkeit periodisch die geschriebenen Daten mit den Parity Daten zu verglichen um Medienfehler und Bitfehler frühzeitig zu erkennen

---
### Verlässlichkeit von Festplatten
* Problem Controller Elektronik
* mehr Kapazität weniger Spielraum für Fehler
* Meantime between Failure (MTBF)
* SelfMonitoringAanalysisReportingTechnology (S.M.A.R.T)
* Unrecoverable Read Error (URE)
* Blackblaze Stats:
https://www.backblaze.com/blog/hard-drive-stats-for-2019/

---
## Meantime between Failure (MTBF)

---
## Medienfehler (SMART)

---
### Unrecoverable Read Error (URE)
#### Es kommt unregelmäßig zu Lesefehlern vom Speichermedium
* Enterprise Disks Bit Error Rate 1 in 10^15 (125TB)
* Hohe Datentransferraten bei RAID Rebuild
* Maßnahme: RAID-6
* Maßnahme: Dateisystem

---
### Unrecoverable Read Error (URE)
#### Beispiel:

Mit einem 65 Terabyte RAID-5 Array bei Enterprise Disks. Wenn eine Disk Ausfällt haben wir eine 50% Wahrscheindlichkeit , dass der Rebuild klappt.

---
### Redundant Array of Independent Disks
#### Zusammenfassung
* Alle Platten müssen gleich groß oder größer sein
* durch große Kapazitäten kann die Reorganisation lange dauern
* ACHTUNG Diskgrößen und Rebuild Zeiten
* ACHTUNG ersetzt kein Backup
* erhöhtes Schreibaktivität

---
### Logical Volume Manager
* aus dem Linux / Unix Umfeld
* unter Windows Dynamische Datenträger 
* Verwaltung von Physischen Disks (PE)
* Zusammenfassen der PE zu Volumes (VE)
* Vergrößern / Verkleinern der logischen Einheiten aus den Volumes (LV)
* kümmert sich um das mapping von Logischen Volumes auf physische Festplatten / Partitionen
* Dient als eine Art Abstraktion von den physischen Disks und dem Dateisystem
* Bieten keine Redundanz

---
### Logical Volume Manager
![](_images/lvm.png)


---
## LVM Features
* Read-only Snapshots
* Read-Write Snapshots
* Thin Provision
* Cache / Tiering


---
### Storage Funktionen - Snapshots
### Ein Snapshot ist eine Momentaufnahme vom Zustand eines Datenspeichers zu einem gewissen Zeitpunkt

---
## Snapshot Eigenschaften
* Auf Blockebene
* Read-Only / Read-Write
* Verwendung zur Unterstützung vom Backup
* Application Aware Snapshots ( Backup / Datenbanken)
#### Beispiele:
LVM / Windows VSS

---
## Storage Funktionen - Snapshots
#### Beispiel LVM (copy on write)

Werden nach einem Snapshot Blöcke in einem Logical Volume geändert wird eine Kopie des Original Blocks in das Snapshot Logical Volume gespeichert und die Änderung am Original Logical Volume geschrieben. Die so entstandene Kopie kann nun lesend für ein Backup verwendet werden.

---
## Storage Funktionen - Snapshots

![LVM Snapshot](_images/lvm-snapshot-cow.png)


---
## Speichersysteme
* DAS
* NAS
* SAN
  
---
### Direct Attached Storage
* Der Speicher wird direkt an den Verbraucher Angebunden
* Ist eine One to One Beziehung
* Schnelle Übertragung der Daten
* Keine Abstraktionsschicht
* Übertragung Blockweise

---
### Direct Attached Storage
#### Protokolle:
* SATA / eSATA
* SAS  
* NVMe   

---
### JBODs
* Just a bunch of Disks
* Keine Erhöung der Ausfallsicherheit
* Die Angeschlossenen Disks werden an das OS weitergereicht
* Keine Verwaltung der Disks
* Verwaltung übernimmt LVM oder das Dateisystem

---
### JBODs Skizze

![jbods](_images/jbods-skizze.png)

---
### JBODs Real
![jbods-ixsystems](_images/2024j_front.png)
![jbods-ixsystems](_images/2024j_back.png)


---
### Network Attached Storage
#### Die Festplatten werden an einen Host Angebunden , dieser Übernimmt die Organisation und ermöglicht vielen Rechnern den Zugriff auf diese Dateien.

---
### Network Attached Storage

* meist Standardhardware (Storage Server)
* Zugriff Filebasierend und Blockweise
* Zugriff (Many to One)
* Zugriff Client – Storage
* NAS Headends für Blockorientierte Storage Systeme
* Spezielle Funktionen on Top (Jails oder Container)

---
### Network Attached Storage
#### Protokolle:
* CIFS / SMB
* NFS
* ISCSI

---
### Storage Area Network
##### Die Festplatten werden an einen Host als Direct Attached Storage Angebunden (Controller) . Dieser Übernimmt die Organisation und ermöglicht vielen Rechnern den Zugriff auf ihren Teil der Daten.

---
### SAN - Eigenschaften
* Spezielle Hardware ( ASIC / FPGA)
* große Entfernungen ( 20 Km)
* Zugriff exclusiv pro Host ( Many to Many )
* Zugriff Server - Storage
* Übertragung Blockorientiert
* LUN ( Logical Unit) System

---
### Storage Area Network
#### Protokolle:
* FiberChannel (over Ethernet)
* ISCSI
* NVMe (over Fabrics)

---
## Storage Area Network
![](_images/san-ueberblick.png)
###### Wikipedia

---
### SAN Storage System "ScaleUP"
 
##### Ein klassisches ScaleUP Storage System besteht aus einem Controller Node , meist mit 2 syncronisierten Controllern (High Availabillity) und ein bis N Disk / Shelf Nodes die mit dem Controller Node via SAS/FC Expander verbunden sind.

---
## SAN Storage System 
#### ScaleUP

![](_images/sam-datacenter-scaleup.png)

---
### SAN Storage System "ScaleUP"
##### Aufbau und Merkmale:

* Controller Node → SAN / NAS → Host
* Controller Node → SAS/FC Expander → Disk / Node  
* IOPS und Disk Kapazität durch die Controller Definiert (SCALEUP)

---
### SAN Storage System "ScaleUP"
##### Beispiele:

* HDS G200
* HP 3PAR
* Purestorage M Serie


---
### SAN Storage System "ScaleOUT"
#### Bei einem ScaleOUT Storage System ist jeder Node ein Controller und Disk/Shelf Node

---
### SAN Storage System "ScaleOUT"

![](_images/sam-datacenter-scaleout.png)



---
### SAN Storage System "ScaleOUT"

* Leistung (IOPS) und Kapazität wächst Linear mit der Nodeanzahl
* Leistungsfähiges Backbone Netzwerk nötig
* oft Objektbasierend (S3)

---
### SAN Storage System "ScaleOUT"
##### Beispiele:

* ScalityRing
* RedhatCEPH
* MiniIO

---
### Kennzahlen von Speichersystemen
* Geschwindigkeit ( Latenz / Durchsatz )
* Datensicherheit
* Verfügbarkeit
* Offen oder geschlossen
* Übertragung Blockorientiert Fileorientiert Objektbasiert
* Funktionen ( Snapshot / Replikation / Multipath)
* Tiering (NVMe /SSD / SAS / Nearline SAS)

---
## Fragen ???

---
## Begriffe
* IOPS (Input/Output Operations in der Sekunde)
* ASIC
* FPGA
* VSS (Volume Shadow Copy Service)
   
---
### Links
* https://de.wikipedia.org/wiki/Linear_Tape_Open
* https://de.wikipedia.org/wiki/Festplattenlaufwerk
* https://de.wikipedia.org/wiki/Self-Monitoring,_Analysis_and_Reporting_Technology
* http://de.wikipedia.org/wiki/RAID
* https://de.wikipedia.org/wiki/NVM_Express
* https://de.wikipedia.org/wiki/Copy-On-Write
* http://www.enterprisestorageforum.com/storage-management/making-raid-work-into-the-future-1.html
* https://www.servethehome.com/raid-calculator/raid-reliability-calculator-simple-mttdl-model/
* http://www.enterprisestorageforum.com/storage-management/making-raid-work-into-the-future-1.html


---
### Fragen ???

- Nennen sie 4 Eigenschaften eines Software RAID Controllers ?
- Skizziere den Aufbau eins Flash Speichers und nenne 3 Merkmale ?
- Nenne die 4 Möglichkeiten von Sparedisks bei RAID Systemen ?
- Nennen sie 4 Aufgaben / Funktionen eines Snapshots ?
- Nenne 4 Storage Funktionen auf Blockebene ?
- Nennen Sie mindestens drei Alleinstellungsmerkmale eines Hardware RAID Controllers ?
- Nennen sie mindestens 4 Eigenschaften eines Logical Volume Managers ?
- Nennen sie 4 Eigenschaften eines Software RAID Controllers ?
- Nennen sie 4 Eigenschaften eines Hardware RAID Controllers ?
- Nennen sie die Funktionsweise eines Snapshots und nenne 3 Eigenschaften ?

---
## Fragen ???
- Nenne 4 Kennzahlen von Storage Systemen ?
- Beschreibe 3 Eigenschaften der syncronen Replikation ?
- Beschreibe ein Scale OUT Storage System und Skizziere es ?
- Nenne 4 Arten von SSD Speichern ?
- Nenne 4 Eigenschaften die die Verlässlichkeit von Festplatten beeinflussen ?
- Beschreibe die syncronen Replikation und nenne 2 Eigenschaften
- Nennen sie 4 Eigenschaften einer Replikation ?
